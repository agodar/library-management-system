package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CheckoutRecord implements Serializable{
	private List<CheckoutRecordEntry> checkoutRecordEntry;
	
	CheckoutRecord(){
		
	}

	public List<CheckoutRecordEntry> getCheckoutRecordEntry() {
		return checkoutRecordEntry;
	}

	public void setCheckoutRecordEntry(List<CheckoutRecordEntry> checkoutRecordEntry) {
		this.checkoutRecordEntry = checkoutRecordEntry;
	}

	public void addEntry(CheckoutRecordEntry checkoutRecordEntry) {
		if(this.checkoutRecordEntry == null)
			this.checkoutRecordEntry = new ArrayList<CheckoutRecordEntry>();
		this.checkoutRecordEntry.add(checkoutRecordEntry);
	}
}
