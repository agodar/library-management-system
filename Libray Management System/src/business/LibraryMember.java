package business;

import java.io.Serializable;
import java.time.LocalDate;

public class LibraryMember extends Person implements Serializable {
	private String memberId;
	private CheckoutRecord checkoutRecord;
	public CheckoutRecord getCheckoutRecord() {
		return checkoutRecord;
	}

	public void setCheckoutRecord(CheckoutRecord checkoutRecord) {
		this.checkoutRecord = checkoutRecord;
	}
	
	public LibraryMember(String firstName, String lastName, String telephone, Address address, String memberId) {
		super(firstName, lastName, telephone, address);
		this.memberId = memberId;
	}
	
	public String getMemberId(){
		return memberId;
	}
	
	
	public void checkout(BookCopy bookCopy,LocalDate todaysDate, LocalDate todaysDatePlusMaxCheckOutLength){
		bookCopy.changeAvailability();
		CheckoutRecordEntry	checkoutRecordEntry = new CheckoutRecordEntry();
		checkoutRecordEntry.createEntry(bookCopy, todaysDate, todaysDatePlusMaxCheckOutLength);
		if(this.checkoutRecord == null)
			checkoutRecord = new CheckoutRecord();
		checkoutRecord.addEntry(checkoutRecordEntry);
	}

}
