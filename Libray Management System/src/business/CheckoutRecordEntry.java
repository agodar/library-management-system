package business;

import java.io.Serializable;
import java.time.LocalDate;

public class CheckoutRecordEntry implements Serializable{
	private LocalDate checkoutDate;
	private LocalDate dueDate;
	private BookCopy bookCopy;
	public BookCopy getBookCopy() {
		return bookCopy;
	}

	public void setBookCopy(BookCopy bookCopy) {
		this.bookCopy = bookCopy;
	}

	public LocalDate getCheckoutDate() {
		return checkoutDate;
	}

	public void setCheckoutDate(LocalDate checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	public LocalDate getDueDate() {
		return dueDate;
	}

	public void setDueDate(LocalDate dueDate) {
		this.dueDate = dueDate;
	}
	
	public void createEntry(BookCopy copy, LocalDate checkoutDate, LocalDate dueDate){
		this.checkoutDate = checkoutDate;
		this.bookCopy = copy;
		this.dueDate = dueDate;
	}
}
