package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import dataaccess.Auth;
import dataaccess.DataAccess;
import dataaccess.DataAccessFacade;
import dataaccess.TestData;
import dataaccess.User;
import business.Address;
import business.Author;
import business.Book;
import business.ControllerInterface;
import business.LibraryMember;
import business.LibrarySystemException;
import business.Person;
import business.SystemController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RouterController implements Initializable {
	@FXML
	private javafx.scene.control.Button homeButton;
	@FXML
	private javafx.scene.control.Button exitButton;
	

	// Add Book instance variables
	@FXML
	private javafx.scene.control.Button saveButton;
	@FXML
	private Text saveMsg;
	@FXML
	private TextField isbn;
	@FXML
	private TextField title;
	@FXML
	private TextField maxCheckOutLength;
	@FXML
	private javafx.scene.control.ListView<String> listOfAuthors;
	
	//MEMBER
	@FXML private TextField memberId;
	@FXML private TextField firstName;
	@FXML private TextField lastName;
	@FXML private TextField phone;
	@FXML private TextField city;
	
	@FXML private TextField street;
	@FXML private TextField zip;
	@FXML private TextField state;
	
	@FXML
	private Button saveMember;
	
	//Members
	
	//End of Members
	
	ListView<String> list = new ListView<String>();
	List<Author> authors = new ArrayList<Author>();

	// End of Add Book

	@FXML
	protected void handleAddMember(ActionEvent event) throws IOException {

		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/addMember.fxml"));
		Scene scene1 = new Scene(pane, 600, 300);
		stg.setTitle("Add Member");
		stg.setScene(scene1);
		stg.show();
	}
	
	
	@FXML protected void handleBookCopy(ActionEvent event) throws IOException {

		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/bookCopy.fxml"));
		Scene scene1 = new Scene(pane, 600, 150);
		stg.setTitle("Book Copy");
		stg.setScene(scene1);
		stg.show();
	}
	
	
	@FXML private TextField isbnSearchField;
	@FXML private Label infoMsg;
	@FXML
	protected void addBookCopy(ActionEvent event) throws IOException {

		String isbnBook = isbnSearchField.getText().toString();
		ControllerInterface sys = new SystemController();
		
		Book bk = sys.searchBook(isbnBook);
		
		try {
			sys.addBookCopy(isbnBook);
			
			infoMsg.setText("Successfully Added Book Copy : Total Copies Now :  "+ bk.getNumCopies());
			
		} catch (LibrarySystemException e) {
			// TODO Auto-generated catch block
			infoMsg.setText(e.getMessage());
		}



	}

	
	
	@FXML private TextField searchTextField;
	@FXML private Button searchMember;
	public void handleSearchMember(ActionEvent event) throws IOException{
		List<LibraryMember> searched = new ArrayList<>();
		for(LibraryMember member: allLibraryMembers){
			if(member.getFirstName().equals(searchTextField.getText()) || member.getLastName().equals(searchTextField.getText()) || member.getMemberId().equals(searchTextField.getText()) ){
				searched.add(member);
			}
		}
		fillMemberTable(searched);
	}
	
	public void fillMemberTable(Collection<LibraryMember> allLibMembers){
		memberTable.getItems().setAll(allLibMembers);
		ObservableList<TableColumn<LibraryMember, ?>> columns= memberTable.getColumns();
		TableColumn<LibraryMember, ?> memberId = columns.get(0);
		TableColumn<LibraryMember, ?> firstName = columns.get(1);
		TableColumn<LibraryMember, ?> lastName = columns.get(2);
		TableColumn<LibraryMember, ?> address = columns.get(4);
		TableColumn<LibraryMember, ?> phone = columns.get(3);
		memberId.setCellValueFactory(new PropertyValueFactory<>("memberId"));
		phone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
		address.setCellValueFactory(new PropertyValueFactory<>("checkoutRecord"));
		firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
		lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
	}
	
	
	TableView<LibraryMember> memberTable;
	Collection<LibraryMember> allLibraryMembers;
	@FXML
	protected void handleMembers(ActionEvent event) throws IOException {
		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/members.fxml"));
		Scene scene1 = new Scene(pane, 600, 300);
		stg.setTitle("List of Members");
		stg.setScene(scene1);
		stg.show();
		//get members and fill table
		DataAccess da = new DataAccessFacade();
		HashMap<String,LibraryMember> hash = da.readMemberMap();
		allLibraryMembers = hash.values();
		memberTable = (TableView<LibraryMember>) scene1.lookup("#usrTable");
		fillMemberTable(allLibraryMembers);

	}
	
	@FXML
	protected void saveMember(ActionEvent event) throws IOException {
		
		String fName = firstName.getText();
		String lName = lastName.getText();
		String pNumber = phone.getText();
		
		
		Address address = new Address(street.getText().toString(), city.getText().toString(), state.getText().toString(), zip.getText().toString());
		//Person per = new Person(firstname.getText().toString(), lastname.getText().toString(), phone.getText().toString(), address);
		Person per = new Person(fName,lName,pNumber, address);
		LibraryMember member = new LibraryMember(fName, lName, pNumber, address, memberId.getText());
		DataAccess access = new DataAccessFacade();
		access.saveNewMember(member);
		Stage stage = (Stage) saveMember.getScene().getWindow();
		// do what you have to do
		stage.close();
		
		handleMembers(event);
		//LoginController loginController = new LoginController();
		//loginController.handleLogin(event);
		
		
	}

	  	@FXML  ListView<String> authorsListView;
		List<Author> lst=new ArrayList<Author>();
		List<Author> authorsList=new ArrayList<Author>();
		List<Book> listBook=new ArrayList<Book>();
		ControllerInterface sysCon=new SystemController();

	@FXML
	protected void handleAddBook(ActionEvent event) throws IOException {
		
       //lblError.setVisible(false);
		
		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/addBook.fxml"));
		Scene scene1 = new Scene(pane, 600, 350);
		stg.setTitle("Add Book");
		stg.setScene(scene1);
		TestData tstData=new TestData();
		lst.addAll(tstData.getAuthors());
		List<String> list = new ArrayList<String>();
		for(Author aut: lst){
			list.add(aut.getFirstName()+" "+ aut.getLastName());
		}
		
		authorsListView = (ListView<String>) scene1.lookup("#authorList");
		
        ObservableList<String> obList = FXCollections.observableList(list);
        authorsListView.getItems().clear();
        authorsListView.setItems(obList);   
        authorsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		stg.show();

	}
	
	@FXML private Button saveBookButton;
	TableView<Book> bookTable;
	Collection<Book> allBooks;
	@FXML
	protected void saveBook(ActionEvent event) throws IOException {

		String isbnBook = isbn.getText().toString();
		String titleBook = title.getText().toString();
		int maxCheckOutLengthBook = Integer.parseInt(maxCheckOutLength.getText());
		
		
		//Address address = new Address("street", "city", "state", "zip");
		//Author author = new Author("Anand", "Godar", "just t", address, "bio");
		//authors.add(author);
		
		 ObservableList<String> selectedItems =  authorsListView.getSelectionModel().getSelectedItems();
         for(String s : selectedItems){
        	 for(Author aut: lst){
     			String name = aut.getFirstName()+" "+ aut.getLastName();
     			if(name.equals(s)){
     				authorsList.add(aut);
     			}
     		}
         }       

		Book book = new Book(isbnBook, titleBook, maxCheckOutLengthBook,
				authorsList);
		DataAccess access = new DataAccessFacade();
		access.saveNewBook(book);
		Stage stage = (Stage) saveBookButton.getScene().getWindow();
		// do what you have to do
		stage.close();
		
		handleBooks(event);
	}
	
	
	@FXML
	protected void handleBooks(ActionEvent event) throws IOException {
		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/books.fxml"));
		Scene scene1 = new Scene(pane, 600, 300);
		stg.setTitle("List of Books");
		stg.setScene(scene1);
		stg.show();
		//get Books and fill table
		DataAccess da = new DataAccessFacade();
		HashMap<String,Book> hash1 = da.readBooksMap();
		allBooks = hash1.values();
		
		bookTable = (TableView<Book>) scene1.lookup("#bkTable");
		
		fillBookTable(allBooks);

	}
	
	public void fillBookTable(Collection<Book> allBooks){
		bookTable.getItems().setAll(allBooks);
		ObservableList<TableColumn<Book, ?>> columns= bookTable.getColumns();
		
		TableColumn<Book, ?> isbn = columns.get(0);
		TableColumn<Book, ?> title = columns.get(1);
		TableColumn<Book, ?> maxCheckOutLength = columns.get(2);
		TableColumn<Book, ?> authors = columns.get(3);
		
		isbn.setCellValueFactory(new PropertyValueFactory<>("isbn"));
		title.setCellValueFactory(new PropertyValueFactory<>("title"));
		maxCheckOutLength.setCellValueFactory(new PropertyValueFactory<>("maxCheckoutLength"));
		authors.setCellValueFactory(new PropertyValueFactory<>("authors"));
		
	}
	
	
	@FXML
	protected void handleCheckOut(ActionEvent event) throws IOException {

		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource("../ui/CheckOut.fxml"));
		Scene scene1 = new Scene(pane, 600, 300);
		stg.setTitle("Check Out Book");
		stg.setScene(scene1);
		stg.show();
	}
	
	@FXML
	protected void handleOverDue(ActionEvent event) throws IOException {

		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource("../ui/OverDue.fxml"));
		Scene scene1 = new Scene(pane, 500, 150);
		stg.setTitle("Over Due");
		stg.setScene(scene1);
		stg.show();
	}
	

	@FXML
	protected void gotohome(ActionEvent event) throws IOException {

		Stage stage = (Stage) homeButton.getScene().getWindow();
		// do what you have to do
		stage.close();

	}

	@FXML
	protected void exitApplication(ActionEvent event) throws IOException {

		Stage stage = (Stage) exitButton.getScene().getWindow();
		// do what you have to do
		stage.close();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}
}
