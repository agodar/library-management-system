package controller;

import java.awt.Label;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import dataaccess.Auth;
import dataaccess.DataAccessFacade;
import business.Address;
import business.Author;
import business.Book;
import business.LoginException;
import business.SystemController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class LoginController implements Initializable {
	@FXML
	private javafx.scene.control.Button loginButton;

	@FXML
	private TextField username;
	@FXML
	private TextField password;
	@FXML
	private Text errorMsg;

	@FXML
	protected void handleLogin(ActionEvent event) throws IOException {
		String user = username.getText();
		String pass = password.getText();

		SystemController sys = new SystemController();
		// DataAccessFacade ad = new DataAccessFacade();
		try {
			sys.login(user, pass);

			Auth auth = new DataAccessFacade().login(user, pass);
				
				String filename="" ;
				
				if(auth==Auth.ADMIN){
					
					filename = "dashboardadmin.fxml";
				}else if(auth==Auth.LIBRARIAN){
					filename = "dashboardlibrarian.fxml";
				}else{
					filename = "dashboard.fxml";
				}
				
				
				// Close login Window
				Stage stage = (Stage) loginButton.getScene().getWindow();
				// do what you have to do
				stage.close();
				// End of login
				Stage stg = new Stage();

				Parent pane = FXMLLoader.load(getClass().getResource(
						"../ui/"+filename));
				Scene scene1 = new Scene(pane, 500, 500);
				stg.setTitle("welcome to LBMS");
				stg.setScene(scene1);
				stg.show();
			

		} catch (LoginException e) {
			errorMsg.setText(e.getMessage());
		}
		// login

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}
}
