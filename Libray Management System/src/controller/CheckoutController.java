package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import business.ControllerInterface;
import business.LibrarySystemException;
import business.SystemController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

public class CheckoutController implements Initializable {
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}
	
	
	@FXML private TextField isbn;
	@FXML private TextField memberId;
	
	@FXML 
	protected void proceedToCheckOut(ActionEvent event) throws IOException {
		ControllerInterface sysC= new SystemController();
		try {
			sysC.checkoutBook(memberId.getText(), isbn.getText());
		} catch (LibrarySystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
