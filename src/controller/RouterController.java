package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import business.Address;
import business.Author;
import business.Book;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class RouterController implements Initializable {
	@FXML
	private javafx.scene.control.Button homeButton;
	@FXML
	private javafx.scene.control.Button exitButton;
	

	// Add Book instance variables
	@FXML
	private javafx.scene.control.Button saveButton;
	@FXML
	private Text saveMsg;
	@FXML
	private TextField isbn;
	@FXML
	private TextField title;
	@FXML
	private TextField maxCheckOutLength;
	List<Author> authors = new ArrayList<Author>();

	// End of Add Book

	@FXML
	protected void handleAddMember(ActionEvent event) throws IOException {

		Stage stg = new Stage();
		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/addMember.fxml"));
		Scene scene1 = new Scene(pane, 600, 300);
		stg.setTitle("Add Member");
		stg.setScene(scene1);
		stg.show();
	}

	@FXML
	protected void handleAddBook(ActionEvent event) throws IOException {

		Stage stg = new Stage();

		Parent pane = FXMLLoader.load(getClass().getResource(
				"../ui/addBook.fxml"));
		Scene scene1 = new Scene(pane, 500, 500);
		stg.setTitle("Add Book");
		stg.setScene(scene1);
		stg.show();

	}

	@FXML
	protected void saveBook(ActionEvent event) throws IOException {

		String isbnBook = isbn.getText().toString();
		String titleBook = title.getText().toString();
		int maxCheckOutLengthBook = Integer.parseInt(maxCheckOutLength
				.getText());
		Address address = new Address("street", "city", "state", "zip");
		Author author = new Author("Anand", "Godar", "just t", address, "bio");
		authors.add(author);
		Book book = new Book(isbnBook, titleBook, maxCheckOutLengthBook,
				authors);
		saveMsg.setText("Saved");
	}

	

	@FXML
	protected void gotohome(ActionEvent event) throws IOException {

		Stage stage = (Stage) homeButton.getScene().getWindow();
		// do what you have to do
		stage.close();

	}

	@FXML
	protected void exitApplication(ActionEvent event) throws IOException {

		Stage stage = (Stage) exitButton.getScene().getWindow();
		// do what you have to do
		stage.close();

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}
}
